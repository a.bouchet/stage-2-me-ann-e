function test() {

    var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName("Actions"); // Récupération de la page Actions sur le Google Sheet : ACTIONS (réponses) v2
    //var ui = SpreadsheetApp.getUi(); // Renvoie une instance de la page Sheet afin d'ajouter des fonctionnalités telles que des menus, boîtes de dialogues ...
    var sas = SpreadsheetApp.getActiveSpreadsheet();
    var sheet = sas.getSheets()[0];

    var r = 2; // Row = Ligne
    var c = 1; // Columns = Colonne
    var i = 1; // Index = Elements de boucle

    var lastColumn = sheet.getLastColumn(); // Récupère la dernière colonne
    var lastRow = sheet.getLastRow(); // Récupère la dernière ligne
    var lastCell = sheet.getRange(lastRow, lastColumn); // Récupère la dernière cellule

    var name = '';
    var stk = ss.getRange(lastRow, 2).getValue(); // Dossier de stockage (semi-racine)
    var name_Racine = stk; // Récupération de la variable stk pour la renommer "name_Racine"
    var folder = DriveApp.getFoldersByName(name_Racine);



    Logger.log(lastCell.getValue());

    for(lastRow;;lastRow++)
    {
        var folder=DriveApp.getFoldersByName(name_Racine);
        var qtn = ss.getRange(lastRow, 3).getValue(); // Quantité souhaitez par l'utilisateur
        var limit = qtn; // variable pour la quantité renommé en "limit"

        while(i < limit + 1) // Tant que i est inférieur à la quantité alors il traite quelle condition il doit réaliser
        {
            var folder=DriveApp.getFoldersByName(name_Racine);
            if(limit == 1)
            {
                var cell = "D" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, 1).getValue(); // Récupération de la valeur de la cellule
                name = v1;
            }
            else if(limit == 2)
            {
                var cell = "E" + lastRow + ":F" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }
            else if(limit == 3)
            {
                var cell = "G" + lastRow + ":I" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }
            else if(limit == 4)
            {
                var cell = "J" + lastRow + ":M" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }
            else if(limit == 5)
            {
                var cell = "N" + lastRow + ":R" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }
            else if(limit == 6)
            {
                var cell = "S" + lastRow + ":X" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }
            else if(limit == 7)
            {
                var cell = "Y" + lastRow + ":AE" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }
            else if(limit == 8)
            {
                var cell = "AF" + lastRow + ":AM" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }
            else if(limit == 9)
            {
                var cell = "AN" + lastRow + ":AV" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }
            else if(limit == 10)
            {
                var cell = "AW" + lastRow + ":BF" + lastRow; // Celulles à traiter
                var range = sheet.getRange(cell); // Renvoie la plage avec la cellule en haut à gauche aux coordonnées données.
                var v1 = range.getCell(1, c).getValue(); // Récupération de la valeur de la cellule
                name = v1;
                c++; // Incrémentation afin que le script traite bien la quantité souhaité
            }

            var folder = DriveApp.getFoldersByName(name_Racine);//.searchFolders("title contains '"+name+"'");
            //var f = DriveApp.getFoldersByName("2020");

            var folder2=null;

            if(folder.hasNext()===true)
            {
                var folder2 = folder.next().searchFolders("title contains '"+name+"'");
            }
            if (folder2.hasNext()===true) {
                while (folder2.hasNext()) {
                    var folder = folder2.next();
                    Logger.log(folder.getName()+' '+folder.getId());
                }
            }else{


                var folder = DriveApp.getFoldersByName(name_Racine);
                if(folder.hasNext()===true)
                {
                    var folder1 = folder.next().createFolder(name); // Création du dossier dans le dossier folder(name_Racine) récupérer plus haut.
                }
                // LES VARIABLES POUR LE PARTAGE

                var SA = 'Z-SA@test.fr';
                var DI = 'Z-DI@test.fr';
                var KITP = 'z-kit-peda@test.fr';
                var TOUS = 'Z-TOUS@test.fr';

                // LES DONNEES D'ENTREES

                var parent=DriveApp.getFolderById(folder1.getId());   // get parent folder
                var folder2 =parent.createFolder('DONNEES-DENTREES');
                var DONNEESDENTREES=DriveApp.getFolderById(folder2.getId());
                DONNEESDENTREES.addViewer(TOUS);
                DONNEESDENTREES.setShareableByEditors(false);

                // KIT ADMI, CREATION FOLDER + OUVERTURE DES DROITS + ON EMPECHE LE PARTAGE PAR LES MODIFIEURS

                var folder5=DONNEESDENTREES.createFolder('KIT-ADMI');
                var KITADMI=DriveApp.getFolderById(folder5.getId());
                KITADMI.setShareableByEditors(false);
                KITADMI.addViewer(TOUS);
                KITADMI.addEditor(SA);
                KITADMI.setShareableByEditors(false);


                // KIT PEDA, CREATION FOLDER + OUVERTURE DES DROITS + ON EMPECHE LE PARTAGE PAR LES MODIFIEURS
                var folder6=DONNEESDENTREES.createFolder('KIT-PEDA');
                var KITPEDA=DriveApp.getFolderById(folder6.getId());

                KITPEDA.addViewer(TOUS);
                KITPEDA.addEditor(DI);
                KITPEDA.addEditor(KITP);
                KITPEDA.setShareableByEditors(false);


                // SUIVI DES ACTIONS

                var folder3=folder1.createFolder('SUIVI-ACTION');

                var SUIVIACTION=DriveApp.getFolderById(folder3.getId());
                SUIVIACTION.addViewer(TOUS);
                SUIVIACTION.setShareableByEditors(false);

                // DEMARRAGE 01

                var folder7=SUIVIACTION.createFolder('DEMARRAGE');
                var DEM=DriveApp.getFolderById(folder7.getId());
                DEM.addViewer(TOUS);
                DEM.setShareableByEditors(false);

                // EMARGEMENTS 02

                var folder26=SUIVIACTION.createFolder('EMARGEMENTS');
                var EMA=DriveApp.getFolderById(folder26.getId());
                EMA.addEditor(TOUS);
                EMA.setShareableByEditors(false);

                // EVALUATION DISPOSITIF 03

                var folder8=SUIVIACTION.createFolder('EVALUATION-DISPOSITIF');
                var ED=DriveApp.getFolderById(folder8.getId());
                ED.addViewer(TOUS);
                ED.setShareableByEditors(false);

                // OUTILS-PEDA   04

                var folder11=SUIVIACTION.createFolder('OUTILS-PEDA');
                var OP=DriveApp.getFolderById(folder11.getId());
                OP.addEditor(TOUS);
                OP.setShareableByEditors(false);

                // PARTENARIATS-VEILLE 05

                var folder12=SUIVIACTION.createFolder('PARTENARIATS-VEILLE');
                var PC=DriveApp.getFolderById(folder12.getId());
                PC.addEditor(TOUS);
                PC.setShareableByEditors(false);


                // SUIVI DISPOSITIF 06

                var folder9=SUIVIACTION.createFolder('SUIVI-DISPOSITIF');
                var SD=DriveApp.getFolderById(folder9.getId());
                SD.addViewer(TOUS);
                SD.setShareableByEditors(false);

                // SUIVI DES ACTIONS >> DEMARRAGE


                var folder13=DEM.createFolder('PLANNING');
                var PG=DriveApp.getFolderById(folder13.getId());
                PG.addEditor(TOUS);
                PG.setShareableByEditors(false);


                var folder14=DEM.createFolder('POSITIONNEMENT');
                var PT=DriveApp.getFolderById(folder14.getId());
                PT.addEditor(TOUS);
                PT.setShareableByEditors(false);


                var folder15=DEM.createFolder('INTEGRATION');
                var IN=DriveApp.getFolderById(folder15.getId());
                    IN.addEditor(TOUS);
                    IN.setShareableByEditors(false);


                var folder17=DEM.createFolder('INFORMATION-COLLECTIVE');
                var IC=DriveApp.getFolderById(folder17.getId());
                IC.addEditor(TOUS);
                IC.setShareableByEditors(false);

                // SUIVI DES ACTIONS >> EVALUATION DISPOSITIF


                var folder18=ED.createFolder('ENQUETE-SATISFACTION');
                var ESN=DriveApp.getFolderById(folder18.getId());
                ESN.addEditor(TOUS);
                ESN.setShareableByEditors(false);


                var folder19=ED.createFolder('BILANS');
                var BLS=DriveApp.getFolderById(folder19.getId());
                BLS.addEditor(TOUS);
                BLS.setShareableByEditors(false);


                var folder20=ED.createFolder('PAS-PAP');
                var PAP=DriveApp.getFolderById(folder20.getId());
                PAP.addEditor(TOUS);
                PAP.setShareableByEditors(false);

                // SUIVI DES ACTIONS >> SUIVI-DISPOSITIF

                var folder21=SD.createFolder('VALIDATION');
                var VAL=DriveApp.getFolderById(folder21.getId());
                VAL.addEditor(TOUS);
                VAL.setShareableByEditors(false);

                var folder22=SD.createFolder('PROGRESSION-PEDAGOGIQUE');
                var PPD=DriveApp.getFolderById(folder22.getId());
                PPD.addEditor(TOUS);
                PPD.setShareableByEditors(false);


                var folder23=SD.createFolder('SUIVI-INDIVIDUEL');
                var SIL=DriveApp.getFolderById(folder23.getId());
                SIL.addEditor(TOUS);
                SIL.setShareableByEditors(false);


                var folder24=SD.createFolder('ENTREPRISE');
                var ENT=DriveApp.getFolderById(folder24.getId());
                ENT.addEditor(TOUS);
                ENT.setShareableByEditors(false);


                var folder25=SD.createFolder('ELECTION-DELEGUES');
                var DELE=DriveApp.getFolderById(folder25.getId());
                DELE.addEditor(TOUS);
                DELE.setShareableByEditors(false);

                // ATTENTION JE SUIS PASSE A FOLDER 26 POUR LA PARTIE INTEGRATION

                i++; // Incrémentation pour atteindre la limite de quantité

                // Redéfinition du propriétaire du dossier et des sous-dossiers
                var p1 = folder1.setOwner("cloud@test.fr");
                var p2 = folder2.setOwner("cloud@test.fr");
                var p3 = folder3.setOwner("cloud@test.fr");
                var p4 = folder5.setOwner("cloud@test.fr");
                var p5 = folder6.setOwner("cloud@test.fr");
                var p6 = folder7.setOwner("cloud@test.fr");
                var p7 = folder8.setOwner("cloud@test.fr");
                var p8 = folder9.setOwner("cloud@test.fr");
                var p9 = folder11.setOwner("cloud@test.fr");
                var p10 = folder12.setOwner("cloud@test.fr");
                var p11 = folder13.setOwner("cloud@test.fr");
                var p12 = folder14.setOwner("cloud@test.fr");
                var p13 = folder15.setOwner("cloud@test.fr");
                var p14 = folder17.setOwner("cloud@test.fr");
                var p15 = folder18.setOwner("cloud@test.fr");
                var p16 = folder19.setOwner("cloud@test.fr");
                var p17 = folder20.setOwner("cloud@test.fr");
                var p18 = folder21.setOwner("cloud@test.fr");
                var p19 = folder22.setOwner("cloud@test.fr");
                var p20 = folder23.setOwner("cloud@test.fr");
                var p21 = folder24.setOwner("cloud@test.fr");
                var p22 = folder25.setOwner("cloud@test.fr");
                var p23 = folder26.setOwner("cloud@test.fr");


            }
        }
        break; // Stop le script après l'action souhaitée
        r++;
        lastRow++;

        break; // Stop le script après l'action souhaitée
    }
}
